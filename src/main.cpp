﻿// Based on custom commands/datarefs sample https://developer.x-plane.com/code-sample/custom-command-with-custom-dataref/


#if APL
#if defined(__MACH__)
#include <Carbon/Carbon.h>
#endif
#endif

#ifndef XPLM210
#error This pluin requires the v2.1 SDK or newer
#endif

#include "XPLMPlugin.h"
#include "XPLMUtilities.h"
#include "XPLMDataAccess.h"
#include "XPLMProcessing.h"

#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include "DRParser.h"

static bool  gOnApt = false;
static DRParser* g_Parser = nullptr;


PLUGIN_API int XPluginStart(
	char *        outName,
	char *        outSig,
	char *        outDesc)
{
	XPLMEnableFeature("XPLM_USE_NATIVE_PATHS", 1);
	gOnApt = false;
	// Plugin Info
	strcpy(outName, "dr-initializer");
	strcpy(outSig, "vitazenisek.plugins.dr-initializer");
	strcpy(outDesc, "This plugin provides a friendly way for data ref initialization on aircraft/livery load.");

	g_Parser = new DRParser();

	return 1;
}


PLUGIN_API void     XPluginStop(void)
{
	if (g_Parser != nullptr)
		delete g_Parser;
	g_Parser = nullptr;
}

PLUGIN_API void XPluginDisable(void)
{
}

PLUGIN_API int XPluginEnable(void)
{
	return 1;
}

PLUGIN_API void XPluginReceiveMessage(XPLMPluginID    /*inFromWho*/,
	long             inMessage,
	void *           inParam)
{
	switch (inMessage) {
	case XPLM_MSG_LIVERY_LOADED:
		//this comes too early during plane load. So check if it was positioned on Airport first
		if (inParam != 0 || !gOnApt)
			return;
		g_Parser->SaveOverrides();
		g_Parser->OnLiveryLoaded();
		break;
	case XPLM_MSG_AIRPORT_LOADED:
		gOnApt = true;
		g_Parser->OnPlaneLoaded();
		break;
	case XPLM_MSG_PLANE_UNLOADED:
		if (inParam != 0 || !gOnApt)
			return;
		g_Parser->SaveOverrides();
		g_Parser->OnPlaneUnloaded();
		break;
	case XPLM_MSG_WILL_WRITE_PREFS:
		g_Parser->SaveOverrides();
	default:
		return;
	}
}

