#pragma once

#include "XPLMDataAccess.h"
#include "XPLMProcessing.h"

#include <vector>
#include <list>
#include <memory>
#include <string>

class DRHandlerBase;

class DRParser {
public:

	struct RowData {
		XPLMDataTypeID _type{ xplmType_Unknown };
		int _i{ 0 };
		float _f{ 0 };
		double _d{ 0 };
		std::vector<int> _vi;
		std::vector<float> _vf;
		std::vector<double> _vd;
		XPLMDataRef _pRef{ nullptr };

		bool SetValuesToDR();
	};

	class Row {
		friend class DRParser;
	public:

		Row(char* buff);
		Row(const Row&) = delete;
		Row(const Row&&) = delete;
		Row& operator=(const Row&) = delete;
		Row& operator=(const Row&&) = delete;
		virtual ~Row();

		bool Parse();

	private:

		DRHandlerBase* NewHandler();
		bool IsExistingDR();
		bool IsOverride() const { return _override; }

		struct ParsedRow {
			const char* _DRName{ nullptr };
			const char* _DRType{ nullptr };
			char* _DRValue{ nullptr };

		};


		char* _buff{ nullptr };
		ParsedRow _rowView;
		std::shared_ptr<RowData> _pData{ new RowData() };
		bool _override{ false };
	};


	DRParser();
	virtual ~DRParser();

	void OnPlaneLoaded();
	void OnPlaneUnloaded();
	void OnLiveryLoaded();
	void SaveOverrides();

private:
	void InitDataRefs(char* filePathBuffer);
	void KillDataRefs();
	bool GetCurrentLiveryPath(char* buffer, const char* filename);
	bool GetCurrentAircraftPath(char* buffer, const char* appendFilename);
	void OnLiveryLoaded_i();
	void ScheduleFLCB();


	std::list<std::shared_ptr<DRHandlerBase> > _handlers;
	std::list<std::string> _overrides;

	static std::list<std::shared_ptr<RowData> > _dr_to_set;
	static float       flcb(
		float                inElapsedSinceLastCall,
		float                inElapsedTimeSinceLastFlightLoop,
		int                  inCounter,
		void *               inRefcon);

	XPLMFlightLoopID _flcbID{ nullptr }; //callback for late setting of existing DataRefs

	XPLMDataRef _LiveryPathDR;

	std::string _LastLiveryPath;
};


