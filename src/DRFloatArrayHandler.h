#ifndef DRFLOATARRAYHANDLER_H
#define DRFLOATARRAYHANDLER_H

#include "DRHandlerBase.h"

#include <vector>

class DRFloatArrayHandler : public DRHandlerBase
{
public:
	DRFloatArrayHandler() = delete;
	DRFloatArrayHandler(const DRFloatArrayHandler&) = delete;
	DRFloatArrayHandler(const DRFloatArrayHandler&&) = delete;
	DRFloatArrayHandler(const char* name, const std::vector<float>&& val);

	int GetFloatArray(
		float *                outValues,
		int                  inOffset,
		int                  inMax) override;

	virtual void SetFloatArray(
		float *                inValues,
		int                  inOffset,
		int                  inCount) override;
protected:
	const char* Name() override;

private:
	std::vector<float> _val;
};

#endif // DRFLOATARRAYHANDLER_H
