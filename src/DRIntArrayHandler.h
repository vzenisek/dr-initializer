#ifndef DRINTARRAYHANDLER_H
#define DRINTARRAYHANDLER_H

#include "DRHandlerBase.h"
#include <vector>

class DRIntArrayHandler : public DRHandlerBase
{
public:
	DRIntArrayHandler() = delete;
	DRIntArrayHandler(const DRIntArrayHandler&) = delete;
	DRIntArrayHandler(const DRIntArrayHandler&&) = delete;
	DRIntArrayHandler(const char* name, const std::vector<int>&& val);

	int GetIntArray(
		int *                outValues,
		int                  inOffset,
		int                  inMax) override;

	void SetIntArray(
		int *                inValues,
		int                  inOffset,
		int                  inCount) override;
protected:
	const char* Name() override;

private:
	std::vector<int> _val;
};

#endif // DRINTARRAYHANDLER_H
