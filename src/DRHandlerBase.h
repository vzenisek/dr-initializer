#ifndef DRHANDLERS_H
#define DRHANDLERS_H

#include "XPLMDataAccess.h"

class DRHandlerBase
{
public:
  virtual ~DRHandlerBase();

  virtual int GetInt();
  virtual void SetInt(int val);

  virtual float GetFloat();
  virtual void SetFloat(float val);

  virtual double GetDouble();
  virtual void SetDouble(double val);

  virtual int GetIntArray(
      int *                outValues,
      int                  inOffset,
      int                  inMax);

  virtual void SetIntArray(
      int *                inValues,
      int                  inOffset,
      int                  inMax);

  virtual int GetFloatArray(
      float *              outValues,
      int                  inOffset,
      int                  inMax);

  virtual void SetFloatArray(
      float *              inValues,
      int                  inOffset,
      int                  inMax);

  static int GetDatai(void* ref);
  static void SetDatai(void* ref, int val);

  static float GetDataf(void* ref);
  static void SetDataf(void* ref, float val);

  static double GetDatad(void* ref);
  static void SetDatad(void* ref, double val);

  static int GetDatavi(void *               inRefcon,
                        int *                outValues,    /* Can be NULL */
                        int                  inOffset,
                        int                  inMax);

  static void SetDatavi(void *               inRefcon,
                        int *                inValues,
                        int                  inOffset,
                        int                  inMax);

  static int GetDatavf(void *               inRefcon,
                        float *                outValues,    /* Can be NULL */
                        int                  inOffset,
                        int                  inMax);

  static void SetDatavf(void *               inRefcon,
                        float *                inValues,
                        int                  inOffset,
                        int                  inMax);

protected:
  DRHandlerBase();
  inline void SetRef(XPLMDataRef ref) { _pRef = ref; }
  virtual const char* Name() = 0;
  void LogWarning(const char* msg);

private:
  XPLMDataRef _pRef{nullptr};

};

#endif // DRHANDLERS_H
