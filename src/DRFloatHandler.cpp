#include "DRFloatHandler.h"

DRFloatHandler::DRFloatHandler(const char* name, float val)
	:_val(val)
{
	XPLMDataRef ref = XPLMRegisterDataAccessor(name, xplmType_Float, true, nullptr, nullptr,
		&DRHandlerBase::GetDataf, &DRHandlerBase::SetDataf,
		nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr,
		this, this);
	SetRef(ref);
}

float DRFloatHandler::GetFloat()
{
	return _val;
}

void DRFloatHandler::SetFloat(float val)
{
	_val = val;
}


const char* DRFloatHandler::Name()
{
	static const char* name = "DRFloatHandler";
	return name;
}
