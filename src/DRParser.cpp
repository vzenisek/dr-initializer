#include "XPLMPlanes.h"
#include "XPLMUtilities.h"

#include "DRParser.h"
#include "Logger.h"
#include "DRHandlerBase.h"
#include "DRIntHandler.h"
#include "DRFloatHandler.h"
#include "DRDoubleHandler.h"
#include "DRIntArrayHandler.h"
#include "DRFloatArrayHandler.h"


#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#ifdef WIN32
#define strcasecmp stricmp
#endif

static char g_Path[2048];

static const char* rd_init_filename = "dr-initializer.dat";
static const char* rd_init_override = "dr-initializer-override.dat";
std::list<std::shared_ptr<DRParser::RowData> > DRParser::_dr_to_set;

DRParser::DRParser()
	:_LiveryPathDR(nullptr)
{
	memset(&g_Path[0], 0, sizeof(g_Path));
	_LiveryPathDR = XPLMFindDataRef("sim/aircraft/view/acf_livery_path");

	XPLMCreateFlightLoop_t t;
	t.structSize = sizeof(XPLMCreateFlightLoop_t);
	t.phase = xplm_FlightLoop_Phase_AfterFlightModel;
	t.refcon = nullptr;
	t.callbackFunc = &flcb;

	_flcbID = XPLMCreateFlightLoop(&t);
}

DRParser::~DRParser()
{
	XPLMDestroyFlightLoop(_flcbID);
	KillDataRefs();
}

void DRParser::KillDataRefs()
{
	_dr_to_set.clear();
	_handlers.clear();
	_overrides.clear();
	_LastLiveryPath.clear();
}

void
DRParser::OnPlaneLoaded()
{
	KillDataRefs();

	OnLiveryLoaded_i();

	ScheduleFLCB();
}

void
DRParser::OnPlaneUnloaded()
{
	KillDataRefs();
}

void
DRParser::OnLiveryLoaded_i()
{
	//Init datarefs from aircraft root
	if (!GetCurrentAircraftPath(g_Path, rd_init_filename)) {
		Logger::Log(Logger::eError, "Can't determine plane path!");
		return;
	}

	InitDataRefs(g_Path);

	//Init datarefs from livery
	if (!GetCurrentLiveryPath(g_Path, rd_init_filename)) {
		Logger::Log(Logger::eWarning, "Can't determine livery path. Using plane root.");
		if (GetCurrentAircraftPath(g_Path, rd_init_override)) {
			_LastLiveryPath = g_Path;
			InitDataRefs(g_Path);
		}
		return;
	}

	InitDataRefs(g_Path);

	//init overrides in livery
	if (!GetCurrentLiveryPath(g_Path, rd_init_override)) {
		return;
	}

	_LastLiveryPath = g_Path;

	InitDataRefs(g_Path);
}

void DRParser::OnLiveryLoaded()
{
	OnLiveryLoaded_i();
	ScheduleFLCB();
}

void DRParser::ScheduleFLCB()
{
	if (_dr_to_set.empty() || _flcbID == nullptr)
		return;
	XPLMScheduleFlightLoop(_flcbID, -50, 1);
}

float       DRParser::flcb(
	float                inElapsedSinceLastCall,
	float                inElapsedTimeSinceLastFlightLoop,
	int                  inCounter,
	void *               inRefcon)
{
	if (_dr_to_set.empty())
		return 0;

	for (auto p : _dr_to_set) {
		p->SetValuesToDR();
	}

	_dr_to_set.clear();
	return 0;
}

FILE* openStorage(const char* filePath, const char* mode);

class FilePtr
{
public:
	FilePtr(FILE* pFile)
		:_file(pFile)
	{
	}

	FilePtr(const FilePtr&) = delete;
	FilePtr& operator =(const FilePtr&) = delete;

	virtual ~FilePtr() {
		if (_file != nullptr)
			fclose(_file);
	}
private:
	FILE* _file{ nullptr };
};

void DRParser::InitDataRefs(char *path)
{
	FILE* pFile = openStorage(path, "r");
	if (!pFile) {
		Logger::Log(Logger::eWarning, "Can't open '%s'", path);
		return;
	}

	FilePtr ptr(pFile);

	char buff[2048];
	int rowCnt = 0;

	while (fgets(buff, 2048, pFile) != nullptr) {
		++rowCnt;
		if (strlen(buff) == 0 || buff[0] == '#')
			continue;
		Row row(buff);
		if (!row.Parse()) {
			Logger::Log(Logger::eError, "Error parsing '%s' : line %d", path, rowCnt);
			continue;
		}
		if (!row.IsExistingDR()) {
			std::shared_ptr<DRHandlerBase> pHandler(row.NewHandler());
			if (pHandler == nullptr) {
				Logger::Log(Logger::eError, "Couldn't create custom dataref handler parsing 's' : line %d", path, rowCnt);
				continue;
			}
			_handlers.push_back(pHandler);
		}
		else {
			if (!row._pData->SetValuesToDR()) {
				Logger::Log(Logger::eError, "Couldn't set dataref values parsing 's' : line %d", path, rowCnt);
				continue;
			}
			//schedule data to set for flcb
			_dr_to_set.push_back(row._pData);
		}
		if (row.IsOverride()) {
			_overrides.push_back(row._rowView._DRName);
		}
	}
}


bool DRParser::GetCurrentLiveryPath(char* buffer, const char* filename)
{
	int numRead = XPLMGetDatab(_LiveryPathDR, buffer, 0, 2048);
	if (numRead > 2048 - static_cast<float>((strlen(filename) + 1)))
		return false; //path longer than buffer? No space for file name anyway...
	buffer[numRead] = '\0';
	if (strlen(buffer) == 0)
		return false;
	strcat(buffer, filename);
	return true;
}

bool DRParser::GetCurrentAircraftPath(char* buffer, const char* appendFile)
{
	static char fileName[256];
	XPLMGetNthAircraftModel(0, fileName, buffer);
	char * file = XPLMExtractFileAndPath(buffer);
	*(file-1) = *XPLMGetDirectorySeparator();
	*file = '\0';
	strcat(buffer, appendFile);
	return true;
}

void DRParser::SaveOverrides()
{
	if (_LastLiveryPath.empty() || _overrides.empty())
		return;

	FILE* pFile = openStorage(_LastLiveryPath.c_str(), "w");
	if (!pFile) {
		Logger::Log(Logger::eWarning, "Can't open '%s'", _LastLiveryPath.c_str());
		return;
	}

	FilePtr ptr(pFile);
	char buff[2048];
	char buff1[256];

	fputs("#Do not edit. This file is automatically generated.\n", pFile);
	
	//it's safer to go with strings
	for (auto drName : _overrides) {
		auto dr = XPLMFindDataRef(drName.c_str());
		if (dr == nullptr)
			continue;
		auto types = XPLMGetDataRefTypes(dr);

		switch (types) {
		case xplmType_Int: 
		{
			auto i = XPLMGetDatai(dr);
			fprintf(pFile, "%s\tINT\t%d\n", drName.c_str(), i);
			break;
		}
		case xplmType_Float:
		{
			auto f = XPLMGetDataf(dr);
			fprintf(pFile, "%s\tFLOAT\t%.6g\n", drName.c_str(), f);
			break;
		}
		case xplmType_Double:
		{
			auto d = XPLMGetDatad(dr);
			fprintf(pFile, "%s\tDOUBLE\t%.6g\n", drName.c_str(), d);
			break;
		}
		case xplmType_FloatArray:
		{
			auto cnt = XPLMGetDatavf(dr, nullptr, 0, 0);
			if (cnt == 0)
				break;

			sprintf(buff, "%s\tFLOAT\t[", drName.c_str());
			for (int i = 0; i < cnt; ++i ) {
				float val = 0.0f;
				XPLMGetDatavf(dr, &val, i, 1);
				sprintf(buff1, "%.6g,", val);
				strcat(buff, buff1);
			}
			strcat(buff, "]\n");
			fputs(buff, pFile);
			break;
		}
		case xplmType_IntArray:
		{
			auto cnt = XPLMGetDatavi(dr, nullptr, 0, 0);
			if (cnt == 0)
				break;

			sprintf(buff, "%s\tINT\t[", drName.c_str());
			for (int i = 0; i < cnt; ++i) {
				int val = 0;
				XPLMGetDatavi(dr, &val, i, 1);
				sprintf(buff1, "%d,", val);
				strcat(buff, buff1);
			}
			strcat(buff, "]\n");
			fputs(buff, pFile);
			break;
		}
		default:
			break;
		};
	}

	_overrides.clear();
}

FILE* openStorage(const char* filePath, const char* mode)
{
	FILE* f = nullptr;

#ifdef WIN32
#include <windows.h>
	//We are using Native path feature, so all paths are UTF-8. For windows, we need to convert them
	static wchar_t wCurrentLiveryPath[1024];
	if (MultiByteToWideChar(CP_UTF8, 0, filePath, -1, &wCurrentLiveryPath[0], 1024) == 0) {
		Logger::Log(Logger::eError, "Can't convert livery path to WCHAR");
		return nullptr;
	}
	static wchar_t wMode[20];
	if (MultiByteToWideChar(CP_UTF8, 0, mode, -1, &wMode[0], 20) == 0) {
		Logger::Log(Logger::eError, "Can't convert mode to WCHAR");
		return nullptr;
	}

	f = _wfopen(wCurrentLiveryPath, wMode);

#else
	f = fopen(filePath, mode);
#endif

	return f;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
/// Row
///
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

template <typename T>
T convertValue(const char* val) {
	T tmp;
	return tmp;
}

template<>
int convertValue(const char* val) {
	return strtol(val, nullptr, 10);
}

template<>
float convertValue(const char* val) {
	return strtof(val, nullptr);
}

template<>
double convertValue(const char* val) {
	return strtod(val, nullptr);
}

template <typename T>
std::vector<T> convertArray(char* arrayStart) {
	std::vector<T> vec;

	char* nextStart = arrayStart + 1;

	while (char* delimiter = strpbrk(nextStart, ",]")) {
		*delimiter = '\0';
		T val = convertValue<T>(nextStart);
		vec.push_back(val);
		nextStart = delimiter + 1;
	}
	return std::move(vec);
}

DRParser::Row::Row(char* buff)
	:_buff(buff)
{

}

DRParser::Row::~Row() {}

bool DRParser::Row::Parse()
{
	if (_buff == nullptr)
		return false;

	//split the buff to three parts. Do not copy
	char* t1 = strchr(_buff, '\t');
	if (t1 == nullptr)
		return false;
	char* t2 = strchr(t1 + 1, '\t');
	if (t2 == nullptr)
		return false;
	char* t3 = strchr(t2 + 1, '\t');
	if (t3 != nullptr) {
		char* ovr = t3 + 1;
		*t3 = '\0';
		_override = *ovr == 'Y' || *ovr == 'y';
	}
	_rowView._DRName = _buff;
	_rowView._DRType = t1 + 1;
	_rowView._DRValue = t2 + 1;
	*t1 = *t2 = '\0';

	char* arrayStart = strchr(_rowView._DRValue, '[');
	if (arrayStart == nullptr) {
        if (strcasecmp(_rowView._DRType, "INT") == 0) {
			_pData->_i = convertValue<int>(_rowView._DRValue);
			_pData->_type = xplmType_Int;
		}
        else if (strcasecmp(_rowView._DRType, "FLOAT") == 0) {
			_pData->_f = convertValue<float>(_rowView._DRValue);
			_pData->_type = xplmType_Float;
		}
        else if (strcasecmp(_rowView._DRType, "DOUBLE") == 0) {
			_pData->_d = convertValue<double>(_rowView._DRValue);
			_pData->_type = xplmType_Double;
		}
		else {
			Logger::Log(Logger::eError, "Can't identify value type.");
			return false;
		}
	}
	else {
        if (strcasecmp(_rowView._DRType, "INT") == 0) {
			_pData->_vi = convertArray<int>(arrayStart);
			_pData->_type = xplmType_IntArray;
		}
        else if (strcasecmp(_rowView._DRType, "FLOAT") == 0) {
			_pData->_vf = convertArray<float>(arrayStart);
			_pData->_type = xplmType_FloatArray;
		}
        else if (strcasecmp(_rowView._DRType, "DOUBLE") == 0) {
			Logger::Log(Logger::eError, "Array of type double not supported by X-Plane datarefs.");
			return false;
		}
		else {
			Logger::Log(Logger::eError, "Can't identify value type.");
			return false;
		}
	}
	return true;
}

DRHandlerBase* DRParser::Row::NewHandler()
{
	DRHandlerBase* pHandler{ nullptr };
	switch (_pData->_type) {
	case xplmType_Int:
		pHandler = new DRIntHandler(_rowView._DRName, _pData->_i);
		break;
	case xplmType_Float:
		pHandler = new DRFloatHandler(_rowView._DRName, _pData->_f);
		break;
	case xplmType_Double:
		pHandler = new DRDoubleHandler(_rowView._DRName, _pData->_d);
		break;
	case xplmType_IntArray:
		pHandler = new DRIntArrayHandler(_rowView._DRName, std::move(_pData->_vi));
		break;
	case xplmType_FloatArray:
		pHandler = new DRFloatArrayHandler(_rowView._DRName, std::move(_pData->_vf));
		break;
	defaut:
		break;
	}
	return pHandler;
}

bool DRParser::Row::IsExistingDR()
{
	if (_pData->_pRef != nullptr)
		return true;

	_pData->_pRef = XPLMFindDataRef(_rowView._DRName);
	return _pData->_pRef != nullptr;
}

bool DRParser::RowData::SetValuesToDR()
{
	if (_pRef == nullptr)
		return false;

	switch (_type) {
	case xplmType_Int:
		XPLMSetDatai(_pRef, _i);
		break;
	case xplmType_Float:
		XPLMSetDataf(_pRef, _f);
		break;
	case xplmType_Double:
		XPLMSetDatad(_pRef, _d);
		break;
	case xplmType_IntArray:
		XPLMSetDatavi(_pRef, _vi.data(), 0, _vi.size());
		break;
	case xplmType_FloatArray:
		XPLMSetDatavf(_pRef, _vf.data(), 0, _vf.size());
		break;
	defaut:
		return false;
	}

	return true;
}


