#include "DRIntArrayHandler.h"
#include <cstring>

DRIntArrayHandler::DRIntArrayHandler(const char* name, const std::vector<int>&& val)
	:_val(val)
{
	XPLMDataRef ref = XPLMRegisterDataAccessor(name, xplmType_IntArray, true, nullptr, nullptr,
		nullptr, nullptr,
		nullptr, nullptr, &DRHandlerBase::GetDatavi, &DRHandlerBase::SetDatavi, nullptr, nullptr, nullptr, nullptr,
		this, this);
	SetRef(ref);
}

int DRIntArrayHandler::GetIntArray(
	int *                outValues,
	int                  inOffset,
	int                  inMax)
{
	if (outValues == nullptr)
		return _val.size();
	if (_val.size() <= inOffset)
		return 0;
	if ((inOffset + inMax) > _val.size())
		inMax = _val.size() - inOffset;
    std::memcpy(outValues, _val.data() + inOffset, inMax * sizeof(int));
	return inMax;
}

void DRIntArrayHandler::SetIntArray(
	int *                inValues,
	int                  inOffset,
	int                  inCount)
{
	if (inValues == nullptr)
		return;
	if (_val.size() <= inOffset)
		return;
	if ((inOffset + inCount) > _val.size())
		inCount = _val.size() - inOffset;
    std::memcpy(_val.data() + inOffset, inValues, inCount * sizeof(int));
}

const char* DRIntArrayHandler::Name()
{
	static const char* name = "DRIntArrayHandler";
	return name;
}

