#include "Logger.h"
#include <cstdio>
#include <cstdarg>
#include <string.h>
#include "XPLMUtilities.h"

char Logger::_buff[2048];


void Logger::Log(Type t, const char* format, ...)
{
	switch (t) {
	case eWarning:
		strcpy(_buff, "dr-initializer: Warning: ");
		break;
	default:
		strcpy(_buff, "dr-initializer: Error: ");
		break;
	};
	char* pBuff = _buff + strlen(_buff);
	va_list args;
	va_start(args, format);
	vsnprintf(pBuff, 2048, format, args);
	va_end(args);
	strcat(_buff, "\n");
	XPLMDebugString(_buff);
}
