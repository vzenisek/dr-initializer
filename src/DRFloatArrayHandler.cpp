#include "DRFloatArrayHandler.h"
#include <cstring>

DRFloatArrayHandler::DRFloatArrayHandler(const char* name, const std::vector<float>&& val)
	:_val(val)
{
	XPLMDataRef ref = XPLMRegisterDataAccessor(name, xplmType_FloatArray, true, nullptr, nullptr,
		nullptr, nullptr,
		nullptr, nullptr, nullptr, nullptr, &DRHandlerBase::GetDatavf, &DRHandlerBase::SetDatavf, nullptr, nullptr,
		this, this);
	SetRef(ref);
}

int DRFloatArrayHandler::GetFloatArray(
	float *                outValues,
	int                  inOffset,
	int                  inMax)
{
	if (outValues == nullptr)
		return _val.size();
	if (_val.size() <= inOffset)
		return 0;
	if ((inOffset + inMax) > _val.size())
		inMax = _val.size() - inOffset;
    std::memcpy(outValues, _val.data() + inOffset, inMax * sizeof(float));
	return inMax;
}

void DRFloatArrayHandler::SetFloatArray(
	float *                inValues,
	int                  inOffset,
	int                  inCount)
{
	if (inValues == nullptr)
		return;
	if (_val.size() <= inOffset)
		return;
	if ((inOffset + inCount) > _val.size())
		inCount = _val.size() - inOffset;
    std::memcpy(_val.data() + inOffset, inValues, inCount * sizeof(float));
}


const char* DRFloatArrayHandler::Name()
{
	static const char* name = "DRFloatArrayHandler";
	return name;
}
