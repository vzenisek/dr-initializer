#include "DRHandlerBase.h"
#include "Logger.h"


DRHandlerBase::DRHandlerBase()
{
}

DRHandlerBase::~DRHandlerBase()
{
	if (_pRef != nullptr) {
		XPLMUnregisterDataAccessor(_pRef);
		_pRef = nullptr;
	}

}

int DRHandlerBase::GetInt()
{
	LogWarning("Accessing int in");
	return 0;
}

void DRHandlerBase::SetInt(int val)
{
	LogWarning("Setting int in");
}

float DRHandlerBase::GetFloat()
{
	LogWarning("Accessing float in");
	return 0;
}

void DRHandlerBase::SetFloat(float val)
{
	LogWarning("Setting float in");
}

double DRHandlerBase::GetDouble()
{
	LogWarning("Accessing double in");
	return 0;
}

void DRHandlerBase::SetDouble(double val)
{
	LogWarning("Setting double in");
}


int DRHandlerBase::GetIntArray(
	int *                outValues,
	int                  inOffset,
	int                  inMax)
{
	LogWarning("Accessing int array in");
	return 0;
}

void DRHandlerBase::SetIntArray(
	int *                inValues,
	int                  inOffset,
	int                  inMax)
{
	LogWarning("Setting int arrray in");
}


int DRHandlerBase::GetFloatArray(
	float *              outValues,
	int                  inOffset,
	int                  inMax)
{
	LogWarning("Accessing float array in");
	return 0;
}

void DRHandlerBase::SetFloatArray(
	float *              inValues,
	int                  inOffset,
	int                  inMax)
{
	LogWarning("Setting float array in");
}

void DRHandlerBase::LogWarning(const char* msg)
{
	Logger::Log(Logger::eWarning, "%s %s", msg, Name());
}

int DRHandlerBase::GetDatai(void* ref)
{
	DRHandlerBase* pBase = static_cast<DRHandlerBase*>(ref);
	if (pBase == nullptr)
		return 0;
	return pBase->GetInt();
}

void DRHandlerBase::SetDatai(void* ref, int val)
{
	DRHandlerBase* pBase = static_cast<DRHandlerBase*>(ref);
	if (pBase == nullptr)
		return;
	pBase->SetInt(val);
}

float DRHandlerBase::GetDataf(void* ref)
{
	DRHandlerBase* pBase = static_cast<DRHandlerBase*>(ref);
	if (pBase == nullptr)
		return 0.0f;
	return pBase->GetFloat();
}

void DRHandlerBase::SetDataf(void* ref, float val)
{
	DRHandlerBase* pBase = static_cast<DRHandlerBase*>(ref);
	if (pBase == nullptr)
		return;
	pBase->SetFloat(val);
}

double DRHandlerBase::GetDatad(void* ref)
{
	DRHandlerBase* pBase = static_cast<DRHandlerBase*>(ref);
	if (pBase == nullptr)
		return 0.0;
	return pBase->GetDouble();
}

void DRHandlerBase::SetDatad(void* ref, double val)
{
	DRHandlerBase* pBase = static_cast<DRHandlerBase*>(ref);
	if (pBase == nullptr)
		return;
	pBase->SetDouble(val);
}

int DRHandlerBase::GetDatavi(void *               inRefcon,
	int *                outValues,    /* Can be NULL */
	int                  inOffset,
	int                  inMax)
{
	DRHandlerBase* pBase = static_cast<DRHandlerBase*>(inRefcon);
	if (pBase == nullptr)
		return 0;
	return pBase->GetIntArray(outValues, inOffset, inMax);
}

void DRHandlerBase::SetDatavi(void *               inRefcon,
	int *                inValues,
	int                  inOffset,
	int                  inMax)
{
	DRHandlerBase* pBase = static_cast<DRHandlerBase*>(inRefcon);
	if (pBase == nullptr)
		return;
	pBase->SetIntArray(inValues, inOffset, inMax);
}

int DRHandlerBase::GetDatavf(void *               inRefcon,
	float *                outValues,    /* Can be NULL */
	int                  inOffset,
	int                  inMax)
{
	DRHandlerBase* pBase = static_cast<DRHandlerBase*>(inRefcon);
	if (pBase == nullptr)
		return 0;
	return pBase->GetFloatArray(outValues, inOffset, inMax);
}

void DRHandlerBase::SetDatavf(void *               inRefcon,
	float *                inValues,
	int                  inOffset,
	int                  inMax)
{
	DRHandlerBase* pBase = static_cast<DRHandlerBase*>(inRefcon);
	if (pBase == nullptr)
		return;
	pBase->SetFloatArray(inValues, inOffset, inMax);
}

