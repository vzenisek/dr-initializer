#ifndef DRDOUBLEHANDLER_H
#define DRDOUBLEHANDLER_H

#include "DRHandlerBase.h"

class DRDoubleHandler : public DRHandlerBase
{
public:
	DRDoubleHandler() = delete;
	DRDoubleHandler(const DRDoubleHandler&) = delete;
	DRDoubleHandler(const DRDoubleHandler&&) = delete;
	DRDoubleHandler(const char* name, double val);

	double GetDouble() override;
	void SetDouble(double val) override;
protected:
	const char* Name() override;

private:
	double _val{ 0.0f };
};

#endif // DRDOUBLEHANDLER_H
