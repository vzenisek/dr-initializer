#ifndef DRINTHANDLER_H
#define DRINTHANDLER_H

#include "DRHandlerBase.h"

class DRIntHandler : public DRHandlerBase
{
public:
	DRIntHandler() = delete;
	DRIntHandler(const DRIntHandler&) = delete;
	DRIntHandler(const DRIntHandler&&) = delete;

	DRIntHandler(const char* name, int val);

	int GetInt() override;
	void SetInt(int val) override;
protected:
	const char* Name() override;

private:
	int _val{ 0 };
};

#endif // DRINTHANDLER_H
