#ifndef DRFLOATHANDLER_H
#define DRFLOATHANDLER_H

#include "DRHandlerBase.h"

class DRFloatHandler : public DRHandlerBase
{
public:
	DRFloatHandler() = delete;
	DRFloatHandler(const DRFloatHandler&) = delete;
	DRFloatHandler(const DRFloatHandler&&) = delete;
	DRFloatHandler(const char* name, float val);

	float GetFloat() override;
	void SetFloat(float val) override;
protected:
	const char* Name() override;

private:
	float _val{ 0.0f };

};

#endif // DRFLOATHANDLER_H
