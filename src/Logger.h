#ifndef LOGGER_H
#define LOGGER_H


class Logger
{
public:
	enum Type {
		eWarning,
		eError
	};

	static void Log(Type t, const char* format, ...);
private:
	static char _buff[2048];
};

#endif // LOGGER_H
