#include "DRDoubleHandler.h"

DRDoubleHandler::DRDoubleHandler(const char* name, double val)
	:_val(val)
{
	XPLMDataRef ref = XPLMRegisterDataAccessor(name, xplmType_Double, true, nullptr, nullptr,
		nullptr, nullptr,
		&DRHandlerBase::GetDatad, &DRHandlerBase::SetDatad, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr,
		this, this);
	SetRef(ref);
}

double DRDoubleHandler::GetDouble()
{
	return _val;
}

void DRDoubleHandler::SetDouble(double val)
{
	_val = val;
}


const char* DRDoubleHandler::Name()
{
	static const char* name = "DRDoubleHandler";
	return name;
}
