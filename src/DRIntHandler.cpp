#include "DRIntHandler.h"

DRIntHandler::DRIntHandler(const char* name, int val)
	:_val(val)
{
	XPLMDataRef ref = XPLMRegisterDataAccessor(name, xplmType_Int, true, &DRHandlerBase::GetDatai, &DRHandlerBase::SetDatai,
		nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr,
		this, this);
	SetRef(ref);
}

int DRIntHandler::GetInt()
{
	return _val;
}

void DRIntHandler::SetInt(int val)
{
	_val = val;
}

const char* DRIntHandler::Name()
{
	static const char* name = "DRIntHandler";
	return name;
}
