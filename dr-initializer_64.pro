# Shared library without any Qt functionality
TEMPLATE = lib
CONFIG -= qt
QT -= gui core

CONFIG += warn_on plugin release
CONFIG -= thread exceptions qt rtti debug

DESTDIR = ./Output
OBJECTS_DIR = ./Intermediate

VERSION = 1.0.0

INCLUDEPATH += ../XPL_SDK/CHeaders/XPLM
INCLUDEPATH += ../XPL_SDK/CHeaders/Wrappers
INCLUDEPATH += ../XPL_SDK/CHeaders/Widgets

# Defined to use X-Plane SDK 2.0 capabilities - no backward compatibility before 9.0
DEFINES += XPLM200 XPLM210

win32 {
    QMAKE_LFLAGS += -static-libgcc
    DEFINES += IBM=1 APL=0
    LIBS += -L../XPL_SDK/Libraries/Win
    LIBS += -lXPLM_64 -lXPWidgets_64
    
    TARGET = win_64.xpl
}

unix:!macx {
    DEFINES += LIN=1
    TARGET = dr-initializer.xpl
    # WARNING! This requires the latest version of the X-SDK !!!!
    QMAKE_CXXFLAGS += -fvisibility=hidden
}

macx {
    DEFINES += APL=1
    TARGET = dr-initializer.xpl
    QMAKE_LFLAGS += -undefined dynamic_lookup#-flat_namespace -undefined suppress




    # Build for multiple architectures.
    # The following line is only needed to build universal on PPC architectures.
    # QMAKE_MAC_SDK=/Developer/SDKs/MacOSX10.4u.sdk
    # The following line defines for which architectures we build.
    CONFIG += x86_64
}

SOURCES += \
    ./src/main.cpp \
    src/DRDoubleHandler.cpp \
    src/DRFloatArrayHandler.cpp \
    src/DRFloatHandler.cpp \
    src/DRHandlerBase.cpp \
    src/DRIntArrayHandler.cpp \
    src/DRIntHandler.cpp \
    src/DRParser.cpp \
    src/Logger.cpp

HEADERS += \
    src/DRDoubleHandler.h \
    src/DRFloatArrayHandler.h \
    src/DRFloatHandler.h \
    src/DRHandlerBase.h \
    src/DRIntArrayHandler.h \
    src/DRIntHandler.h \
    src/DRParser.h \
    src/Logger.h
