dr-initializer plugin by Vita Zenisek
https://bitbucket.org/vzenisek/dr-initializer/src/master/

This plugin reads data ref definition from aircraft root and livery folder and sets their initial value.
If data ref does not exist, it creates custom one.

There are two levels of ini files. One on aircraft root, other on livery.
All files has to be named 'dr-initializer.dat'

Data file format:
# means line comment. Valid on the line start only.
<dataref name>\t<dataref type>\t<initial value>[\t<override>]

<dataref type> can be INT FLOAT DOUBLE
<initial value> Single value or array.
<override> is optional. If Y or y is specified, value of the dataref will be stored in dr-initializer-override.dat in the livery and last value of the dataref is remembered.
			This file is generated only if there are overrides defined. File is generated on X-Plane exit, livery change or airplane reload.

Note: arrays of type DOUBLE are not supported by X-Plane

How overrides work:
When airplane or livery is loaded, datarefs are initialized in following order:
If livery folder exists:
1) dr-initializer.dat in plane root
2) dr-initializer.dat in livery folder
3) dr-initializer-override.dat  in livery folder. This file is created automatically whenever overrides are defined in any dr-initializer.dat

If livery folder does not exists for a livery (default livery):
1) dr-initializer.dat in plane root
2) dr-initializer-override.dat in plane root. This file is created automatically whenever overrides are defined in any dr-initializer.dat

Examples:
#simple custom dataref
Z242/myref1	INT	10
#array-typecustom dataref of floats
Z242/myref2	FLOAT	[2.3,4.5,8.2]
#array-typedataref of floats defined by X-Plane
sim/flightmodel/weight/m_fuel	FLOAT	[5.2,6.8,7.9]
#example with override
Z242/myref3	FLOAT	[2.6,4.7]	Y


There is no UI available, as it is intended for use by aircraft developers.

Look for "dr-initializer:" in x-plane log first if something does not work.

Feel free to use it with any aircraft model, including commercial ones.

Enjoy!
